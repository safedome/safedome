Safedome connects to your phone via Bluetooth LE 5. Once you have successfully connected to the app, let Safedome keep an eye on the location of your items and alert you if they go out of range. Keep Safedome in your wallet, purse, handbag or passport. Connected to the app youll always know the location of your items if they ever go missing.

Website: https://safedome.com/
